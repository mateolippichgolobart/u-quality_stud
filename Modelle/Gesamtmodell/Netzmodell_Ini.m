% Skript zur Initialisierung des Netzmodells

clear all;
close all;
clc;

%Einheitenvorsaetze
k = 1e3;
m = 1e-3;
u = 1e-6;

%Simulationsparameter
Tsample = 100*u;                                % Rechenraster in s
Tsim = 10;                                      % Simulationszeit in s

% Wahl der Lasten
Last_Unsymmetrie = 0;                           % Last Unsymmetrie      1=on 0=off 
Last_Oberschwingung = 1;                        % Last Oberschwingung   1=on 0=off             
Last_RVC = 0;                                   % Last RVC              1=on 0=off   
Last_Flicker = 0;                               % Last Flicker          1=on 0=off   
Last_Grundlast = 1;                             % Last Grundlast        1=on 0=off   

% UPQC Shunt Series on/off

UPQC_Shunt = 0;                                 % UPQC Shunt        1=on 0=off
UPQC_Series = 1;                                % UPQC Series       1=on 0=off

UPQC_Shunt_time = [1 2 3];                      % Zeitpunkte Schalter auf zu Shunt
UPQC_Shunt_Amp = [0 1 0];                       % An entsprechendem Zeitpunkt on/off Shunt on=1 off=0

UPQC_Series_time = [1 2 3];                     % Zeitpunkte Schalter auf zu Series
UPQC_Series_Amp = [0 1 0];                      % An entsprechendem Zeitpunkt on/off Series on=1 off=0

% UPQC Filter Auswahl

UPQC_Filter_Shunt = 0;                          % UPQC Filter Auswahl 0=L; 1=LC; 2=LCL
UPQC_Filter_Series = 1;                         % UPQC Filter Auswahl 0=L; 1=LC; 2=LCL



%% Netzmodell
    %Netz
    Us_LL = 400;                                % LeiterLeiter-Spannung nominal
    Us_Amp = 326;                               % Leiter-Erde-Spannung nominal
    f_nom = 50;                                 % 50 Hertz Netzfrequenz

    %Spannungsquelle
    Us = [Us_Amp; Us_Amp; Us_Amp];              % Amplitude der Quellenspannung [V]
    Phis = [0; -120; 120];                      % Phase L1, L2, L3 [°]   
    fs = [f_nom; f_nom; f_nom];                 % Frequenz L1, L2, L3 [Hz]

    Rs_in = 10.88*m; %8.23*m;                   % Innenwiderstand Spg-Q in Ohm
    Ls_in = 73.75*u; %77.056*u;                 % Inneninduktivitaet Spq-Q in H
    
    % R/X = 0,47 % 0,34  --> sehr induktiv
    
    %Umrechnung in p.u.
    Vpu = 1/(Us_LL*sqrt(2)/sqrt(3));
  
    %Leitungsnachbildung
    Rtrans = 0.206;                             % Widerstandsbelag in Ohm/km
    Ltrans = 0.256*m;                           % Induktivitaetsbelag in H/km
    strans = 0.25;                              % Leitungslaenge in km
       
%% Lasten Parameter

%Last Unsymmetrie (Uns)                             
if(Last_Unsymmetrie == 1)
    cosphi_Uns = 0.9;                               %Leistungsfaktor
    
    Pl_Uns = [5; 1; 1]*k;                           % Wirkleistung L1, L2, L3 [kW]
    
        QLL1_Uns = sqrt((Pl_Uns(1)/cosphi_Uns)^2-Pl_Uns(1)^2);  %Blindleistung induktiv Leiter 1 in Var
        QLL2_Uns = sqrt((Pl_Uns(2)/cosphi_Uns)^2-Pl_Uns(2)^2);  %Blindleistung induktiv Leiter 2 in Var 
        QLL3_Uns = sqrt((Pl_Uns(3)/cosphi_Uns)^2-Pl_Uns(3)^2);  %Blindleistung induktiv Leiter 3 in Var
    
    Ql_ind_Uns = [QLL1_Uns; QLL2_Uns; QLL3_Uns];                % Blindleistung L1, L2, L3 [Var]
    
        QCL1_Uns = 0;                               %Blindleistung kapazitiv Leiter 1 in Var
        QCL2_Uns = 0;                               %Blindleistung kapazitiv Leiter 1 in Var
        QCL3_Uns = 0;                               %Blindleistung kapazitiv Leiter 1 in Var
    
    Ql_kap_Uns = [QCL1_Uns; QCL2_Uns; QCL3_Uns];
    
    fak_Uns = [1; 1; 1];                            % Faktor fuer Lasterhoehung L1, L2, L3
end

%Last RVC (RVC)
if(Last_RVC == 1)
    cosphi_RVC = 0.9;                               %Leistungsfaktor
    
    Pl_RVC = [5; 1; 1]*k;                           % Wirkleistung L1, L2, L3 [kW]
    
        QLL1_RVC = sqrt((Pl_RVC(1)/cosphi_RVC)^2-Pl_RVC(1)^2);  %Blindleistung induktiv Leiter 1 in Var
        QLL2_RVC = sqrt((Pl_RVC(2)/cosphi_RVC)^2-Pl_RVC(2)^2);  %Blindleistung induktiv Leiter 2 in Var 
        QLL3_RVC = sqrt((Pl_RVC(3)/cosphi_RVC)^2-Pl_RVC(3)^2);  %Blindleistung induktiv Leiter 3 in Var
    
    Ql_ind_RVC = [QLL1_RVC; QLL2_RVC; QLL3_RVC];                % Blindleistung L1, L2, L3 [Var]
    
        QCL1_RVC = 0;                               %Blindleistung kapazitiv Leiter 1 in Var
        QCL2_RVC = 0;                               %Blindleistung kapazitiv Leiter 1 in Var
        QCL3_RVC = 0;                               %Blindleistung kapazitiv Leiter 1 in Var
    
    Ql_kap_RVC = [QCL1_RVC; QCL2_RVC; QCL3_RVC];
    
    fak_RVC = [1; 1; 1];                            % Faktor fuer Lasterhoehung L1, L2, L3
end

%Last Oberschwingung (Ober)
if(Last_Oberschwingung == 1)
    I_Basis_Ober_L1 = 200;                      %Basisstrom_Amp L1 [A]
    I_Basis_Ober_L2 = 200;                      %Basisstrom_Amp L2 [A]
    I_Basis_Ober_L3 = 200;                      %Basisstrom_Amp L3 [A]
    
    I_Basis_Ober = [I_Basis_Ober_L1; I_Basis_Ober_L2; I_Basis_Ober_L3];    %Basisstrom_Amp L1,L2,L3 [A]
    
    I_relativ_Ober_L1 = [100  0.33 19.73 0.25 14.39 0.29 8.88 0.40 4.72 0.30 3.46 0.10 4.26 0.14 1.05 0.05 0.85 0.04 0.51 0.03 0.24 0.02 0.21 0.02 0.24 0.02 0.15 0.03 0.07 0.02 0.14 0.02 0.08 0.02 0.03 0.02 0.05 0.01 0.06 0.01 0.06 0.01 0.05 0.01 0.06 0.01 0.04 0.01 0.02 0.01];    % relativer Anteil von Basisstrom L1 [%] weitere Erklärung siehe S.
    I_relativ_Ober_L2 = [100  0.33 19.73 0.25 14.39 0.29 8.88 0.40 4.72 0.30 3.46 0.10 4.26 0.14 1.05 0.05 0.85 0.04 0.51 0.03 0.24 0.02 0.21 0.02 0.24 0.02 0.15 0.03 0.07 0.02 0.14 0.02 0.08 0.02 0.03 0.02 0.05 0.01 0.06 0.01 0.06 0.01 0.05 0.01 0.06 0.01 0.04 0.01 0.02 0.01];   % relativer Anteil von Basisstrom L2 [%] weitere Erklärung siehe S.
    I_relativ_Ober_L3 = [100  0.33 19.73 0.25 14.39 0.29 8.88 0.40 4.72 0.30 3.46 0.10 4.26 0.14 1.05 0.05 0.85 0.04 0.51 0.03 0.24 0.02 0.21 0.02 0.24 0.02 0.15 0.03 0.07 0.02 0.14 0.02 0.08 0.02 0.03 0.02 0.05 0.01 0.06 0.01 0.06 0.01 0.05 0.01 0.06 0.01 0.04 0.01 0.02 0.01];    % relativer Anteil von Basisstrom L3 [%] weitere Erklärung siehe S.
    
    I_relativ_Ober = [I_relativ_Ober_L1; I_relativ_Ober_L2; I_relativ_Ober_L3]; % relativer Anteil von Basisstrom L1,L2,L3 [%] weitere Erklärung siehe S.
    
    Vielfache_Ober_L1 = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50]; %Vielfache auf 50Hz normiert L1[] weitere Erklärung siehe S.
    Vielfache_Ober_L2 = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50]; %Vielfache auf 50Hz normiert L2[] weitere Erklärung siehe S.
    Vielfache_Ober_L3 = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50]; %Vielfache auf 50Hz normiert L3[] weitere Erklärung siehe S.
    
    Vielfache_Ober = [Vielfache_Ober_L1; Vielfache_Ober_L2; Vielfache_Ober_L3]; %Vielfache auf 50Hz normiert L1,L2,L3[] weitere Erklärung siehe S.
    
    Phi_Ober_L1 = -25;                %Phase L1 [°] 
    Phi_Ober_L2 = Phi_Ober_L1-120;    %Phase L2 [°] 
    Phi_Ober_L3 = Phi_Ober_L1+120;    %Phase L3 [°] 
    
    Phi_Ober = [Phi_Ober_L1; Phi_Ober_L2; Phi_Ober_L3]; %Phase L1,L2,L3 [°] 
    
end            

%Last Flicker (Flick)
if(Last_Flicker == 1)
    I_Basis_Flick_L1 = 50;                      %Basisstrom_Amp L1 [A]
    I_Basis_Flick_L2 = 50;                      %Basisstrom_Amp L2 [A]
    I_Basis_Flick_L3 = 50;                      %Basisstrom_Amp L3 [A]
    
    I_Basis_Flick = [I_Basis_Flick_L1; I_Basis_Flick_L2; I_Basis_Flick_L3];    %Basisstrom_Amp L1,L2,L3 [A]
    
    I_relativ_Flick_L1 = [100 0.1 10 0.1 0.1 0.1 1 16 5 1.1 2.5 3 4 2.8 6.2 2.8 1.1 0.9 0.2 3 0.6 0.7 7 2.1 5 2.5 2.7 4 0.4 5 25 3 1.1 1.2 2 0.2 0.3 1];    % relativer Anteil von Basisstrom L1 [%] weitere Erklärung siehe S.
    I_relativ_Flick_L2 = [100 0.1 10 0.1 0.1 0.1 1 16 5 1.1 2.5 3 4 2.8 6.2 2.8 1.1 0.9 0.2 3 0.6 0.7 7 2.1 5 2.5 2.7 4 0.4 5 25 3 1.1 1.2 2 0.2 0.3 1];    % relativer Anteil von Basisstrom L2 [%] weitere Erklärung siehe S.
    I_relativ_Flick_L3 = [100 0.1 10 0.1 0.1 0.1 1 16 5 1.1 2.5 3 4 2.8 6.2 2.8 1.1 0.9 0.2 3 0.6 0.7 7 2.1 5 2.5 2.7 4 0.4 5 25 3 1.1 1.2 2 0.2 0.3 1];    % relativer Anteil von Basisstrom L3 [%] weitere Erklärung siehe S.
    
    I_relativ_Flick = [I_relativ_Flick_L1; I_relativ_Flick_L2; I_relativ_Flick_L3]; % relativer Anteil von Basisstrom L1,L2,L3 [%] weitere Erklärung siehe S.
    
    Vielfache_Flick_L1 = [1 2e-3 0.01 0.02 0.04 0.06 0.08 0.1 0.12 0.14 0.16 0.18 0.2 0.22 0.24 0.26 0.28 0.3 0.32 0.34 0.36 0.38 0.4 0.42 0.44 0.46 0.48 0.5 0.52 0.54 0.56 0.58 0.6 0.62 0.64 0.66 0.68 0.7]; %Vielfache auf 50Hz normiert L1[] weitere Erklärung siehe S.
    Vielfache_Flick_L2 = [1 2e-3 0.01 0.02 0.04 0.06 0.08 0.1 0.12 0.14 0.16 0.18 0.2 0.22 0.24 0.26 0.28 0.3 0.32 0.34 0.36 0.38 0.4 0.42 0.44 0.46 0.48 0.5 0.52 0.54 0.56 0.58 0.6 0.62 0.64 0.66 0.68 0.7]; %Vielfache auf 50Hz normiert L2[] weitere Erklärung siehe S.
    Vielfache_Flick_L3 = [1 2e-3 0.01 0.02 0.04 0.06 0.08 0.1 0.12 0.14 0.16 0.18 0.2 0.22 0.24 0.26 0.28 0.3 0.32 0.34 0.36 0.38 0.4 0.42 0.44 0.46 0.48 0.5 0.52 0.54 0.56 0.58 0.6 0.62 0.64 0.66 0.68 0.7]; %Vielfache auf 50Hz normiert L3[] weitere Erklärung siehe S.
    
    Vielfache_Flick = [Vielfache_Flick_L1; Vielfache_Flick_L2; Vielfache_Flick_L3]; %Vielfache auf 50Hz normiert L1,L2,L3[] weitere Erklärung siehe S.
    
    Phi_Flick_L1 = -25;                 %Phase L1 [°] 
    Phi_Flick_L2 = Phi_Flick_L1-120;    %Phase L2 [°] 
    Phi_Flick_L3 = Phi_Flick_L1+120;    %Phase L3 [°] 
    
    Phi_Flick = [Phi_Flick_L1; Phi_Flick_L2; Phi_Flick_L3]; %Phase L1,L2,L3 [°] 
    
end  
    
%Last Grundlast (Grund)
if(Last_Grundlast == 1)
    cosphi_Grund = 0.9;                               %Leistungsfaktor
    
    Pl_Grund = [5; 1; 1]*k;                           % Wirkleistung L1, L2, L3 [kW]
    
        QLL1_Grund = sqrt((Pl_Grund(1)/cosphi_Grund)^2-Pl_Grund(1)^2);  %Blindleistung induktiv Leiter 1 in Var
        QLL2_Grund = sqrt((Pl_Grund(2)/cosphi_Grund)^2-Pl_Grund(2)^2);  %Blindleistung induktiv Leiter 2 in Var 
        QLL3_Grund = sqrt((Pl_Grund(3)/cosphi_Grund)^2-Pl_Grund(3)^2);  %Blindleistung induktiv Leiter 3 in Var
    
    Ql_ind_Grund = [QLL1_Grund; QLL2_Grund; QLL3_Grund];                % Blindleistung L1, L2, L3 [Var]
    
        QCL1_Grund = 0;                               %Blindleistung kapazitiv Leiter 1 in Var
        QCL2_Grund = 0;                               %Blindleistung kapazitiv Leiter 1 in Var
        QCL3_Grund = 0;                               %Blindleistung kapazitiv Leiter 1 in Var
    
    Ql_kap_Grund = [QCL1_Grund; QCL2_Grund; QCL3_Grund];    
end  

%% Filterparameter Shunt

%L1
L_Eingang_Shunt_L1 = 1*m;                                    % L UPQC Seite  für L, LC, LCL-Filter Parameter setzen [H]
R_Eingang_Shunt_L1 = 0.1;                                    % R UPQC Seite  für L, LC, LCL-Filter Parameter setzen [Ohm]

L_Ausgang_Shunt_L1 = 1*m;                                    % L Netz Seite  für LCL-Filter Parameter setzen [H]
R_Ausgang_Shunt_L1 = 0.1;                                    % R Netz Seite  für LCL-Filter Parameter setzen [Ohm]

C_parallel_Shunt_L1 = 100*u;                                 % C zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [H]
R_parallel_Shunt_L1 = 0;                                     % R zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [Ohm]

%L2
L_Eingang_Shunt_L2 = 1*m;                                    % L UPQC Seite  für L, LC, LCL-Filter Parameter setzen [H]
R_Eingang_Shunt_L2 = 0.1;                                    % R UPQC Seite  für L, LC, LCL-Filter Parameter setzen [Ohm]

L_Ausgang_Shunt_L2 = 1*m;                                    % L Netz Seite  für LCL-Filter Parameter setzen [H]
R_Ausgang_Shunt_L2 = 0.1;                                    % R Netz Seite  für LCL-Filter Parameter setzen [Ohm]

C_parallel_Shunt_L2 = 100*u;                                 % C zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [H]
R_parallel_Shunt_L2 = 0;                                     % R zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [Ohm]

%L3
L_Eingang_Shunt_L3 = 1*m;                                    % L UPQC Seite  für L, LC, LCL-Filter Parameter setzen [H]
R_Eingang_Shunt_L3 = 0.1;                                    % R UPQC Seite  für L, LC, LCL-Filter Parameter setzen [Ohm]

L_Ausgang_Shunt_L3 = 1*m;                                    % L Netz Seite  für LCL-Filter Parameter setzen [H]
R_Ausgang_Shunt_L3 = 0.1;                                    % R Netz Seite  für LCL-Filter Parameter setzen [Ohm]

C_parallel_Shunt_L3 = 100*u;                                 % C zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [H]
R_parallel_Shunt_L3 = 0;                                     % R zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [Ohm]

%N
L_Eingang_Shunt_N = 1*m;                                    % L UPQC Seite  für L, LC, LCL-Filter Parameter setzen [H]
R_Eingang_Shunt_N = 0.1;                                    % R UPQC Seite  für L, LC, LCL-Filter Parameter setzen [Ohm]

L_Ausgang_Shunt_N = 1*m;                                    % L Netz Seite  für LCL-Filter Parameter setzen [H]
R_Ausgang_Shunt_N = 0.1;                                    % R Netz Seite  für LCL-Filter Parameter setzen [Ohm]


%% Filterparameter Series

%L1
L_Eingang_Series_L1 = 1*m;                                    % L UPQC Seite  für L, LC, LCL-Filter Parameter setzen [H]
R_Eingang_Series_L1 = 0.1;                                    % R UPQC Seite  für L, LC, LCL-Filter Parameter setzen [Ohm]

L_Ausgang_Series_L1 = 1*m;                                    % L Netz Seite  für LCL-Filter Parameter setzen [H]
R_Ausgang_Series_L1 = 0.1;                                    % R Netz Seite  für LCL-Filter Parameter setzen [Ohm]

C_parallel_Series_L1 = 100*u;                                 % C zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [H]
R_parallel_Series_L1 = 0;                                     % R zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [Ohm]

%L2
L_Eingang_Series_L2 = 1*m;                                    % L UPQC Seite  für L, LC, LCL-Filter Parameter setzen [H]
R_Eingang_Series_L2 = 0.1;                                    % R UPQC Seite  für L, LC, LCL-Filter Parameter setzen [Ohm]

L_Ausgang_Series_L2 = 1*m;                                    % L Netz Seite  für LCL-Filter Parameter setzen [H]
R_Ausgang_Series_L2 = 0.1;                                    % R Netz Seite  für LCL-Filter Parameter setzen [Ohm]

C_parallel_Series_L2 = 100*u;                                 % C zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [H]
R_parallel_Series_L2 = 0;                                     % R zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [Ohm]

%L3
L_Eingang_Series_L3 = 1*m;                                    % L UPQC Seite  für L, LC, LCL-Filter Parameter setzen [H]
R_Eingang_Series_L3 = 0.1;                                    % R UPQC Seite  für L, LC, LCL-Filter Parameter setzen [Ohm]

L_Ausgang_Series_L3 = 1*m;                                    % L Netz Seite  für LCL-Filter Parameter setzen [H]
R_Ausgang_Series_L3 = 0.1;                                    % R Netz Seite  für LCL-Filter Parameter setzen [Ohm]

C_parallel_Series_L3 = 100*u;                                 % C zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [H]
R_parallel_Series_L3 = 0;                                     % R zwischen UPQC und Netz parallel für LC, LCL-Filter Parameter setzen [Ohm]

%N
L_Eingang_Series_N = 1*m;                                    % L UPQC Seite  für L, LC, LCL-Filter Parameter setzen [H]
R_Eingang_Series_N = 0.1;                                    % R UPQC Seite  für L, LC, LCL-Filter Parameter setzen [Ohm]

L_Ausgang_Series_N = 1*m;                                    % L Netz Seite  für LCL-Filter Parameter setzen [H]
R_Ausgang_Series_N = 0.1;                                    % R Netz Seite  für LCL-Filter Parameter setzen [Ohm]

%% Simulinkmodell öffnen
open_system('Netzmodell');                                          % öffnet Simulink Modell
disp('Simulink Netzmodell öffnen')                                  % Rückmeldung in Konsole
%% Lasten aus/ein-kommentieren
if(Last_Unsymmetrie == 1)                                           % Last Unsymmetrie on/off
set_param('Netzmodell/Unsymmetrie Last','commented','off');
else 
set_param('Netzmodell/Unsymmetrie Last','commented','on');
end

if(Last_Oberschwingung == 1)                                        % Last Oberschwingunge on/off
set_param('Netzmodell/Oberschwingung Last','commented','off');
else 
set_param('Netzmodell/Oberschwingung Last','commented','on');
end

if(Last_RVC == 1)                                                   % Last RVC on/off
set_param('Netzmodell/RVC Last','commented','off');
else 
set_param('Netzmodell/RVC Last','commented','on');
end

if(Last_Flicker == 1)                                               % Last Flicker on/off
set_param('Netzmodell/Flicker Last','commented','off');
else 
set_param('Netzmodell/Flicker Last','commented','on');
end

if(Last_Grundlast == 1)                                             % Last Grundlast on/off
set_param('Netzmodell/Grundlast','commented','off');
else 
set_param('Netzmodell/Grundlast','commented','on');
end

%% Filter aus/ein-kommentieren Shunt

if(UPQC_Filter_Shunt == 0)                                              % 0 = L-Filter                                   
set_param('Netzmodell/L Filter Shunt','commented','off');               % Schalte L Filter ein

set_param('Netzmodell/LC Filter Shunt','commented','on');               % kommentiere LC Filter aus
set_param('Netzmodell/LCL Filter Shunt','commented','on');              % kommentiere LCL Filter aus
elseif (UPQC_Filter_Shunt == 1)                                         % 1 = LC-Filter  
set_param('Netzmodell/LC Filter Shunt','commented','off');              % Schalte LC Filter ein

set_param('Netzmodell/L Filter Shunt','commented','on');                % kommentiere L Filter aus
set_param('Netzmodell/LCL Filter Shunt','commented','on');              % kommentiere LCL Filter aus
elseif (UPQC_Filter_Shunt == 2)                                         % 2 = L-Filter  
set_param('Netzmodell/LCL Filter Shunt','commented','off');             % Schalte LCL Filter ein

set_param('Netzmodell/LC Filter Shunt','commented','on');               % kommentiere LC Filter aus
set_param('Netzmodell/L Filter Shunt','commented','on');                % kommentiere L Filter aus
end

%% Filter aus/ein-kommentieren Series

if(UPQC_Filter_Series == 0)                                              % 0 = L-Filter                                   
set_param('Netzmodell/L Filter Series','commented','off');               % Schalte L Filter ein

set_param('Netzmodell/LC Filter Series','commented','on');               % kommentiere LC Filter aus
set_param('Netzmodell/LCL Filter Series','commented','on');              % kommentiere LCL Filter aus
elseif (UPQC_Filter_Series == 1)                                         % 1 = LC-Filter  
set_param('Netzmodell/LC Filter Series','commented','off');              % Schalte LC Filter ein

set_param('Netzmodell/L Filter Series','commented','on');                % kommentiere L Filter aus
set_param('Netzmodell/LCL Filter Series','commented','on');              % kommentiere LCL Filter aus
elseif (UPQC_Filter_Series == 2)                                         % 2 = L-Filter  
set_param('Netzmodell/LCL Filter Series','commented','off');             % Schalte LCL Filter ein

set_param('Netzmodell/LC Filter Series','commented','on');               % kommentiere LC Filter aus
set_param('Netzmodell/L Filter Series','commented','on');                % kommentiere L Filter aus
end

%% UPQC Shunt on/off

if(UPQC_Shunt == 0)
set_param('Netzmodell/Three-Phase Breaker Shunt','InitialState','open')
set_param('Netzmodell/N Breaker Shunt','InitialState','0')
elseif(UPQC_Shunt == 1)
set_param('Netzmodell/Three-Phase Breaker Shunt','InitialState','closed')    
set_param('Netzmodell/N Breaker Shunt','InitialState','1')
else
disp('Fehler! UPQC Shunt on/off anderer Wert als 1 oder 0 gewählt')
end

%% UPQC Series on/off

if(UPQC_Series == 1)
set_param('Netzmodell/L1 Breaker Series','InitialState','0')
set_param('Netzmodell/L2 Breaker Series','InitialState','0')
set_param('Netzmodell/L3 Breaker Series','InitialState','0')

set_param('Netzmodell/L1a Breaker Series','InitialState','1')
set_param('Netzmodell/L2a Breaker Series','InitialState','1')
set_param('Netzmodell/L3a Breaker Series','InitialState','1')

elseif(UPQC_Series == 0)    
set_param('Netzmodell/L1 Breaker Series','InitialState','1')
set_param('Netzmodell/L2 Breaker Series','InitialState','1')
set_param('Netzmodell/L3 Breaker Series','InitialState','1')

set_param('Netzmodell/L1a Breaker Series','InitialState','0')
set_param('Netzmodell/L2a Breaker Series','InitialState','0')
set_param('Netzmodell/L3a Breaker Series','InitialState','0')
else
disp('Fehler! UPQC Series on/off anderer Wert als 1 oder 0 gewählt')
end



%% keine Fehler Ausgabe

disp('Ini Script keine Fehler')                                         % Ende Script keine Fehler Ausgabe
