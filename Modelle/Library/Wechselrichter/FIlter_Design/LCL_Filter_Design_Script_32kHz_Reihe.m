% System parameters
Pn = 45084; %Inverter Power(W)
En = 520; % Grid voltage (V)
Vdc = 720; %DC link voltage (V)
fn = 50; %Grid frequency (Hz)
wn = 2*pi*fn;
fsw = 32000; %Switching frequency (Hz)
wsw = 2*pi*fsw;
% Base values
Zb = (En^2)/Pn
Cb = 1/(wn*Zb)
% Filter parameters
delta_Ilmax=0.05*((Pn*sqrt(2))/En)
Li=Vdc/(16*fsw*delta_Ilmax) %Inverter side inductance
x = 0.05;
Cf = x*Cb %Filter capacitor
% Calculation of the factor,r,between Linv and Lg
r = 0.2;
% Grid side inductance (including transformer inductance)
%Lg = r*Li
Lg = 42.7e-6
% Calculation of wres,resonance frequency of the filter
wres = sqrt((Li+Lg)/(Li*Lg*Cf));
fres=wres/(2*pi)
%Damping resistance
Rd = 1/(3*wres*Cf)

G = tf([1],[Li*Cf*Lg 0 (Li+Lg) 0]);
G_damped = tf([Cf*Rd 1],[Li*Cf*Lg Cf*(Li+Lg)*Rd (Li+Lg) 0]);
bode(G,G_damped),grid